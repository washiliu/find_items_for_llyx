package com.aaa.service;

import com.aaa.entity.Address;
import com.aaa.entity.Fuser;
import com.aaa.entity.Orders;
import com.aaa.entity.ShopCar;
import com.aaa.util.DefaultMsg;

import java.util.List;

public interface FuserService {
    //根据账号查询用户
    Fuser findByFuname(String username);
    //加入购物车
    DefaultMsg addShopCar(ShopCar shopCar);
    //查询我的购物车
    List<ShopCar> listShopCar(Long fuid);
    //注册用户
    DefaultMsg addFuserCar(Fuser fuser);
    //移除购物车
    DefaultMsg removeShopCar(String str,Long uid);
    //修改数量
    DefaultMsg updateStock(Long pid,Long num,Long uid);
    //清空购物车
    DefaultMsg cleanShopCar(Long uid);
    //查询购物车数量
    Integer ShopCarCount(Long uid);
    //生成订单和订单详情
    DefaultMsg saveorders(String abc,String pids,Long uid);
    //查看我的订单
    List<Orders> listOrders(Long uid,Long ostate,String keyword);
    //修改订单状态 待发货
    DefaultMsg updateOrderStates(Long oid);
    //修改订单状态 已收货
    DefaultMsg updateOrderStatesSh(Long oid);
    //查询我的地址
    List<Address> listAllAddress(Long uid);
    //查询默认地址
    Address findByMoren(Long uid);
    //新增收货地址
    DefaultMsg saveOrUpdateAddress(Address address);
    //删除收货地址
    DefaultMsg deleteAddress(Long uid,Long aid);
    //修改地址状态
    DefaultMsg updateDefault(Address address,Long uid);
    //修改密码校验原密码是否正确
    DefaultMsg checkOldPassword(String uname,String password);
    //修改密码
    DefaultMsg updatePassword(String password,Long uid);
    //修改头像
    DefaultMsg updateImgPath(String imgPath,Long uid);
    //删除我的订单
    DefaultMsg deleteMyOrder(Long uid ,Long oid);
    //根据订单id查询订单
    Orders findByOid(Long oid);
    
}
