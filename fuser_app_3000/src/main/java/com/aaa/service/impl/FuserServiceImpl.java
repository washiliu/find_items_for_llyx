package com.aaa.service.impl;

import cn.hutool.core.date.DateUtil;
import com.aaa.dao.FuserDao;
import com.aaa.entity.*;
import com.aaa.service.FeginProductServie;
import com.aaa.service.FuserService;
import com.aaa.util.DefaultMsg;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class FuserServiceImpl implements FuserService {
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Resource
    private FuserDao fuserDao;
    @Autowired
    private FeginProductServie feginProductServie;

    //根据账号查询用户
    @Override
    public Fuser findByFuname(String fuid) {
        return fuserDao.findByFuname(fuid);
    }


    //加入购物车
    @Override
    public DefaultMsg addShopCar(ShopCar shopCar) {
        DefaultMsg msg = new DefaultMsg();
        //查询用户购物车内此商品的数量
        Integer o = (Integer) redisTemplate.opsForHash().get(shopCar.getFuser().getFuid(), String.valueOf(shopCar.getProduct().getPid()));
        //如果购物车中此商品数量不为空
        if (o != null) {
//          数量加一
            o += 1;
        } else {
//          如果为空 没有此商品 数量等于1
            o = 1;
        }
        redisTemplate.opsForHash().put(shopCar.getFuser().getFuid(), String.valueOf(shopCar.getProduct().getPid()), o);
        return msg;
    }

    /**
     * 查询我的购物车
     *
     * @param fuid
     * @return
     */
    @Override
    public List<ShopCar> listShopCar(Long fuid) {
        Map entries = redisTemplate.opsForHash().entries(fuid);
        List<ShopCar> shopCarList = new ArrayList<ShopCar>();
        for (Object key : entries.keySet()) {
            System.out.println(key);
            //k商品id v 商品数量
            Fuser fuser = new Fuser();
            ShopCar shopCar = new ShopCar();
            //根据商品id 查询商品信息
            Product product = feginProductServie.findByPid(Long.valueOf(key.toString()));
            shopCar.setNum(Long.valueOf(entries.get(key).toString()));
            shopCar.setProduct(product);
            fuser.setFuid(fuid);
            shopCar.setFuser(fuser);
            shopCarList.add(shopCar);
        }

        return shopCarList;
    }

    /**
     * 修改购物车库存
     *
     * @param pid
     * @param num
     * @param uid
     * @return
     */
    @Override
    public DefaultMsg updateStock(Long pid, Long num, Long uid) {
        DefaultMsg defaultMsg = new DefaultMsg();
        redisTemplate.opsForHash().put(uid, String.valueOf(pid), num);
        return defaultMsg;
    }

    /**
     * 移除购物车
     *
     * @param
     * @param uid
     * @return
     */
    @Override
    public DefaultMsg removeShopCar(String str, Long uid) {
        String[] split = str.split(",");
        Long delete = 0L;
        for (String pid : split) {
            delete = redisTemplate.opsForHash().delete(uid, pid);
        }
        DefaultMsg defaultMsg = new DefaultMsg();
        if (delete == 0) {
            defaultMsg.setMsg("移除失败");
            defaultMsg.setSuccess(0);
        }
        return defaultMsg;
    }


    /**
     * 清空购物车
     *
     * @param uid
     * @return
     */
    @Override
    public DefaultMsg cleanShopCar(Long uid) {
        Boolean delete = redisTemplate.delete(uid);
        DefaultMsg defaultMsg = new DefaultMsg();
        if (!delete) {
            defaultMsg.setMsg("清空失败");
            defaultMsg.setSuccess(0);
        }
        return defaultMsg;
    }

    /**
     * 查询购物车数量
     *
     * @param uid
     * @return
     */
    @Override
    public Integer ShopCarCount(Long uid) {
        //查出来所有商品
        Map entries = redisTemplate.opsForHash().entries(uid);
        //循环遍历  购物车中 每个商品数量
        Integer bb = 0;
        for (Object aa : entries.values()) {
            bb += Integer.valueOf(aa.toString());
        }

        return bb;
    }

    /**
     * 注册用户
     *
     * @param fuser
     * @return
     */
    @Override
    public DefaultMsg addFuserCar(Fuser fuser) {
        fuser.setFustate(Long.valueOf(1));
        fuser.setPassword(new BCryptPasswordEncoder().encode(fuser.getPassword()));
        fuser.setCreateDate(DateUtil.now());
        Integer integer = fuserDao.addFuserCar(fuser);
        DefaultMsg defaultMsg = new DefaultMsg();
        if (integer == null) {
            defaultMsg.setMsg("注册失败");
            defaultMsg.setSuccess(0);
        }
        return defaultMsg;
    }

    /**
     * 生成订单
     *
     * @param abc  拼凑的地址信息
     * @param pids 拼凑的商品id
     * @param uid  用户id
     * @return
     */
    @Override
    public DefaultMsg saveorders(String abc, String pids, Long uid) {
        //获取当前时间
        //创建时间
        String createDate = DateUtil.now();
        //存储要生成的订单id
        Orders orders = new Orders();
        //组装订单详情数据集合
        List<OrderList> orderSList = new ArrayList<OrderList>();
        //商品的id集合
        String[] pidArray = pids.split(",");
        //声明订单总金额
        Integer money = 0;
        for (String pid : pidArray) {
            //0用户id 1商品id 2数量 3价格 4地址
            orders.setAddress(abc.substring(9, abc.length()));
            //根据商品id查出商品的信息
            Product product = feginProductServie.findByPid(Long.valueOf(pid));
            //根据商品id 查出购物车中 相对应商品的数量
            Integer num = (Integer) redisTemplate.opsForHash().get(uid, pid);
            /**
             * 商品详细数据
             */
            OrderList orderList = new OrderList();
            //对应商品的数量
            orderList.setCount(Long.valueOf(num));
            //对应商品的id
            orderList.setPid(product.getPid());
            //对应商品的单价
            orderList.setPrice(String.valueOf(product.getPrice()));
            orderSList.add(orderList);

            /**
             * 查询地址
             */
            Address byMoren = fuserDao.findByMoren(uid);
            /**
             * 订单数据
             */
            //订单总价 因为String 0.0不能转换Integer 要截取
            money += num * Integer.valueOf(product.getPrice().toString().substring(0, product.getPrice().toString().length() - 2));
            orders.setPhone(byMoren.getPhone());
            orders.setPerson(byMoren.getPerson());
            orders.setCreateDate(createDate);
            orders.setOstate(Long.valueOf(0));
            orders.setOuid(uid);
        }
        orders.setCprice(String.valueOf(money));
        Integer integer = fuserDao.saveOrders(orders);
        if (integer == 0) {
            throw new RuntimeException("保存订单失败");
        } else {
            rabbitTemplate.convertAndSend("normalOrderExchange", "", orders);
        }
        //保存订单详情
        Long oid = orders.getOid();
        for (OrderList list : orderSList) {
            list.setOid(oid);
            Integer integer1 = fuserDao.saveOrdersList(list);
            if (integer1 == 0 || integer == null) {
                throw new RuntimeException("订单详情保存错误");
            }
        }
        //移除购物车中对应的信息
        for (String pid : pidArray) {
            Long delete = redisTemplate.opsForHash().delete(uid, pid);
            if (delete == 0) {
                throw new RuntimeException("移除购物车信息出错");
            }
        }
        //更改 减少库存
        for (OrderList orderItem : orderSList) {
            Product byPname = feginProductServie.findByPid(orderItem.getPid());
            Product product = new Product();
            product.setPid(orderItem.getPid());
            product.setStock(byPname.getStock() - orderItem.getCount());
            Integer update = feginProductServie.update(product);
            if (update == 0) {
                throw new RuntimeException("更改商品库存错误");
            }
        }
        DefaultMsg defaultMsg = new DefaultMsg();
        defaultMsg.setTarget(orders);
        return defaultMsg;
    }

    /**
     * 查看我的订单
     *
     * @param uid
     * @return
     */
    @Override
    public List<Orders> listOrders(Long uid, Long ostate, String keyword) {
        List<Orders> orders = new ArrayList<>();
        if (keyword != "" || keyword != null) {
            //如果有关键字 先根据关键字 和用户id查询 包含商品 的订单详情
            List<OrderList> orderLists = fuserDao.listOredrByKeword(uid, keyword);
            for (OrderList orderList : orderLists) {
                //拿着订单详情的 用订单id查询订单
                Orders orders1 = fuserDao.ByOid(orderList.getOid(), ostate);
                orders.add(orders1);
            }
        } else {
            //没有关键字 直接根据所有订单
            orders = fuserDao.listOrders(uid, ostate);
        }
        //移除 数组中的空值
        orders.removeAll(Collections.singleton(null));
        //如果 数组为空 则返回空
        if (CollectionUtils.isNotEmpty(orders)) {
            //遍历所有查出的所有订单 并查出 对应的订单详情
            for (Orders orders1 : orders) {
                List<OrderList> orderLists = fuserDao.listOrdersList(orders1.getOid());
                orders1.setOrderList(orderLists);
            }
        }
        return orders;
    }

    /**
     * 修改订单状态待发货
     *
     * @param oid
     * @param
     * @return
     */
    @Override
    public DefaultMsg updateOrderStates(Long oid) {
        DefaultMsg msg = new DefaultMsg();
        Integer integer = fuserDao.updateOrderState(oid, Long.valueOf(1));
        if (integer == null) {
            msg.setMsg("失败");
            msg.setSuccess(0);
        }
        return msg;
    }

    /**
     * 修改订单状态已收货
     *
     * @param oid
     * @return
     */
    @Override
    public DefaultMsg updateOrderStatesSh(Long oid) {
        DefaultMsg msg = new DefaultMsg();
        Integer integer = fuserDao.updateOrderState(oid, Long.valueOf(3));
        if (integer == null) {
            msg.setMsg("失败");
            msg.setSuccess(0);
        }
        return msg;
    }

    /**
     * 查询我的地址
     *
     * @param uid
     * @return
     */
    @Override
    public List<Address> listAllAddress(Long uid) {
        return fuserDao.listAllAddress(uid);
    }

    @Override
    public Address findByMoren(Long uid) {
        return fuserDao.findByMoren(uid);
    }

    /**
     * 新增或修改地址
     *
     * @param address
     * @return
     */
    @Override
    public DefaultMsg saveOrUpdateAddress(Address address) {
        Long aid = address.getAid();
        Integer count = 0;
        if (aid == null) {
            //如果没有默认地址 那么添加为默认地址
            if (fuserDao.findByMoren(address.getUid()) == null) {
                address.setIsDefault(Long.valueOf(1));
            } else {
                address.setIsDefault(Long.valueOf(0));
            }
            count = fuserDao.saveAddress(address);
        } else {
            count = fuserDao.updateAddress(address);
        }
        DefaultMsg msg = new DefaultMsg();
        if (count == 0) {
            msg.setSuccess(0);
            msg.setMsg("操作失败");
        }
        return msg;
    }

    /**
     * 删除地址
     *
     * @param uid
     * @param aid
     * @return
     */
    @Override
    public DefaultMsg deleteAddress(Long uid, Long aid) {
        Integer integer = fuserDao.deleteAddress(uid, aid);
        DefaultMsg defaultMsg = new DefaultMsg();
        if (integer == null) {
            defaultMsg.setMsg("删除失败");
            defaultMsg.setSuccess(0);
        }
        return defaultMsg;
    }

    @Override
    public DefaultMsg updateDefault(Address address, Long uid) {
        //先查询是否有默认地址 如果没有 直接把选中的设为默认
        Address byMoren = fuserDao.findByMoren(uid);
        if (byMoren == null) {
            address.setUid(uid);
            address.setIsDefault(1L);
            fuserDao.updateDefault(address);
        } else {
            // 把原来的默认地址更改为0
            byMoren.setIsDefault(0L);
            Integer integer = fuserDao.updateDefault(byMoren);
            if (integer > 0) {
                // 如果更换成功  把选中的选项改为默认的地址 1
                address.setIsDefault(1L);
                Integer integer1 = fuserDao.updateDefault(address);
                if (integer1 != 1) {
                    throw new RuntimeException("更换默认地址失败");
                }
            } else {
                throw new RuntimeException("取消原来默认地址失败");
            }
        }

        return new DefaultMsg();
    }

    /**
     * 修改密码校验原密码是否正确
     *
     * @param password
     * @return
     */
    @Override
    public DefaultMsg checkOldPassword(String uname, String password) {
        Fuser byFuname = fuserDao.findByFuname(uname);
        String oldPassword = byFuname.getPassword();
        boolean matches = new BCryptPasswordEncoder().matches(password, oldPassword);
        DefaultMsg defaultMsg = new DefaultMsg();
        if (!matches) {
            defaultMsg.setMsg("原密码错误");
            defaultMsg.setSuccess(0);
        }
        return defaultMsg;
    }

    /**
     * 修改密码
     *
     * @param password
     * @param uid
     * @return
     */
    @Override
    public DefaultMsg updatePassword(String password, Long uid) {
        String newpassword = new BCryptPasswordEncoder().encode(password);
        Integer integer = fuserDao.updatePassword(uid, newpassword);
        DefaultMsg defaultMsg = new DefaultMsg();
        if (integer == null) {
            defaultMsg.setMsg("修改失败");
            defaultMsg.setSuccess(0);
        }
        return defaultMsg;
    }

    /**
     * 修改用户头像
     *
     * @param imgPath
     * @param uid
     * @return
     */
    @Override
    public DefaultMsg updateImgPath(String imgPath, Long uid) {
        Integer integer = fuserDao.updateImg(imgPath, uid);
        DefaultMsg defaultMsg = new DefaultMsg();
        if (integer == null) {
            defaultMsg.setMsg("修改头像失败");
            defaultMsg.setSuccess(0);
        }
        return defaultMsg;
    }

    /**
     * 删除我的订单
     *
     * @param oid
     * @param uid
     * @return
     */
    @Override
    public DefaultMsg deleteMyOrder(Long uid, Long oid) {
        Integer integer = fuserDao.deleteMyOrder(uid, oid);
        DefaultMsg defaultMsg = new DefaultMsg();
        if (integer == 0) {
            defaultMsg.setMsg("删除失败");
            defaultMsg.setSuccess(0);
        }
        return defaultMsg;
    }

    /**
     * 根据订单id查询订单
     *
     * @param oid
     * @return
     */
    @Override
    public Orders findByOid(Long oid) {
        return fuserDao.findByOid(oid);
    }
}
