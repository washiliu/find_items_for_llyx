package com.aaa.service;

import com.aaa.entity.Product;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("fproduct-app")
public interface FeginProductServie {
    @RequestMapping("/pro/findByPid")
    public Product findByPid(@RequestParam(value = "pid") Long pid);

    @RequestMapping("/pro/update")
    public Integer update(@RequestBody Product product);
}
