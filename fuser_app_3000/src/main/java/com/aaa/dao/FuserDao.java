package com.aaa.dao;

import com.aaa.entity.*;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface FuserDao {
    //根据账号密码查询
    Fuser findByFuname(String username);

    //修改密码
    Integer updatePassword(@Param("uid") Long uid, @Param("password") String password);

    //加入购物车
    Integer addShopCar(ShopCar shopCar);

    //查询我的购物车
    List<ShopCar> listShopCar(Long fuid);

    //注册用户
    Integer addFuserCar(Fuser fuser);

    //移除购物车
    Integer removeShopCar(@Param("pid") Long pid, @Param("uid") Long uid);

    ////修改购物数量
    Integer updateStock(@Param("pid") Long pid, @Param("num") Long num, @Param("uid") Long uid);

    //根据商品id查询购物车的商品数据
    ShopCar findByPid(@Param("pid") Long pid, @Param("uid") Long uid);

    //清空购物车
    Integer cleanShopCar(Long uid);

    //查询我的购物车数量
    Integer shopCarCount(Long uid);

    //生成订单
    Integer saveOrders(Orders orders);

    //生成订单详情
    Integer saveOrdersList(OrderList orderList);

    //查询我的订单
    List<Orders> listOrders(@Param("uid") Long uid, @Param("ostate") Long ostate);

    //根据订单id查询订单详情
    List<OrderList> listOrdersList(Long oid);

    //根据id查询订单
    Orders ByOid(@Param("oid") Long oid,@Param("ostate") Long ostate);

    //根据关键字查询我的订单详情
    List<OrderList> listOredrByKeword(@Param("uid") Long uid, @Param("keyword") String keyword);

    //修改订单状态
    Integer updateOrderState(@Param("oid") Long oid, @Param("state") Long state);

    //查询我的地址
    List<Address> listAllAddress(Long uid);

    //查询我的默认地址
    Address findByMoren(Long uid);

    //新增收货地址
    Integer saveAddress(Address address);

    //编辑收货地址
    Integer updateAddress(Address address);

    //更改地址状态
    Integer updateDefault(Address address);
    //删除收货地址

    Integer deleteAddress(@Param("uid") Long uid, @Param("aid") Long aid);

    //修改头像
    Integer updateImg(@Param("imgPath") String imgPath, @Param("uid") Long uid);

    //删除我的订单
    Integer deleteMyOrder(@Param("uid") Long uid, @Param("oid") Long oid);

    //根据订单id查询订单状态
    Orders findByOid(Long oid);
}
