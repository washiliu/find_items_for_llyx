package com.aaa.controller;

import com.aaa.entity.*;
import com.aaa.service.FuserService;
import com.aaa.util.DefaultMsg;
import com.aaa.util.PayUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/fu")
public class FuserController {
    @Resource
    private FuserService fuserService;

    @RequestMapping("/toLogin")
    public String toLogin() {
        return "login";
    }

    private Object getUser(Authentication authentication) {
        Object principal = authentication.getPrincipal();
        Fuser byFuname = fuserService.findByFuname(principal.toString());
        return byFuname;
    }

    /*
     * 退出登录 后干的阿巴阿巴
     * */
    @RequestMapping("/MyLogout")
    public DefaultMsg logout() {
        return new DefaultMsg();
    }

    /**
     * 根据姓名查询我的信息
     * @param authentication
     * @return
     */
    @RequestMapping("/findByName")
    public Fuser findByname(Authentication authentication) {
        Object principal = authentication.getPrincipal();
        Fuser byFuname = fuserService.findByFuname(principal.toString());
        return byFuname;
    }

    /**
     * 根据姓名查询我的信息(注册校验)
     * @param
     * @return
     */
    @RequestMapping("/findByFname")
    public DefaultMsg findByfname(String fname) {
        DefaultMsg defaultMsg = new DefaultMsg();
        Fuser byFuname = fuserService.findByFuname(fname);
        if (byFuname != null) {
            defaultMsg.setMsg("该用户已存在");
            defaultMsg.setSuccess(0);
        }
        return defaultMsg;
    }

    /**
     * 修改密码校验原密码是否正确
     * @param authentication
     * @param password
     * @return
     */
    @RequestMapping("/checkOldPassword")
    public DefaultMsg CheckPassword(Authentication authentication, String password) {
        Fuser fuser = (Fuser) this.getUser(authentication);
        DefaultMsg defaultMsg = fuserService.checkOldPassword(fuser.getUsername(), password);
        return defaultMsg;
    }

    /**
     * 修改密码
     * @param authentication
     * @param password
     * @return
     */
    @RequestMapping("/updatePassword")
    public DefaultMsg updatePassword(Authentication authentication, String password) {
        Fuser fuser = (Fuser) this.getUser(authentication);
        DefaultMsg defaultMsg = fuserService.updatePassword(password, fuser.getFuid());
        return defaultMsg;
    }

    /**
     * 修改头像
     * @param imgPath
     * @param authentication
     * @return
     */
    @RequestMapping("/updateImg")
    public DefaultMsg updateImg(String imgPath, Authentication authentication) {
        Fuser fuser = (Fuser) this.getUser(authentication);
        DefaultMsg defaultMsg = fuserService.updateImgPath(imgPath, fuser.getFuid());
        return defaultMsg;
    }

    /**
     * 加入购物车
     * @param
     * @return
     */
    @RequestMapping("/addShopCar")
    public DefaultMsg addShopCar(Authentication authentication, Product product) {
        Fuser fuser = (Fuser) this.getUser(authentication);
        ShopCar shopCar = new ShopCar();
        shopCar.setFuser(fuser);
        shopCar.setProduct(product);
        DefaultMsg defaultMsg = fuserService.addShopCar(shopCar);
        return defaultMsg;
    }

    /**
     * 查询我的购物车
     * @param authentication
     * @return
     */
    @RequestMapping("/listShopCar")
    public List<ShopCar> listShopCar(Authentication authentication) {
        Fuser fuser = (Fuser) this.getUser(authentication);
        return fuserService.listShopCar(fuser.getFuid());
    }

    /**
     * 注册用户
     * @param fuser
     * @return
     */
    @RequestMapping("/addFuser")
    public DefaultMsg addFuservice(Fuser fuser) {
        DefaultMsg defaultMsg = fuserService.addFuserCar(fuser);
        return defaultMsg;
    }

    /**
     * 移除购物车
     * @return
     */
    @RequestMapping("/removeShopCar")
    public DefaultMsg removeShopCar(String str, Authentication authentication) {
        Fuser fuser = (Fuser) this.getUser(authentication);
        DefaultMsg defaultMsg = fuserService.removeShopCar(str, fuser.getFuid());
        return defaultMsg;
    }

    /**
     * 修改购买数量
     * @param
     * @return
     */
    @RequestMapping("/updateStock")
    public DefaultMsg updateStock(Long pid, Long uid, Long num) {
        DefaultMsg defaultMsg = fuserService.updateStock(pid, num, uid);
        return defaultMsg;
    }

    /**
     * 清空购物车
     * @param authentication
     * @return
     */
    @RequestMapping("/cleanShopCar")
    public DefaultMsg cleanShopCar(Authentication authentication) {
        Fuser fuser = (Fuser) this.getUser(authentication);
        DefaultMsg defaultMsg = fuserService.cleanShopCar(fuser.getFuid());
        return defaultMsg;
    }

    /**
     * 购物车条数
     * @param authentication
     * @return
     */
    @RequestMapping("/shopCarCount")
    public Integer shopCarCount(Authentication authentication) {
        Fuser user = (Fuser) this.getUser(authentication);
        Integer integer = fuserService.ShopCarCount(user.getFuid());
        return integer;
    }

    /**
     * 生成订单和订单详情
     * @param
     * @return
     */
    @RequestMapping("/saveorders")
    public DefaultMsg saveorders(String abc,String pids,Authentication authentication) {
        Fuser user = (Fuser) this.getUser(authentication);
        return  fuserService.saveorders(abc,pids,user.getFuid());
    }

    /**
     * 查询我的订单
     *
     * @param authentication
     * @return
     */
    @RequestMapping("/listMyOrders")
    public PageInfo<Orders> listOrders(Authentication authentication,Long ostate, Integer currentPage, Integer pageSize, String keyword) {
        Fuser user = (Fuser) this.getUser(authentication);
        PageHelper.startPage(currentPage,pageSize);
        List<Orders> ordersList = fuserService.listOrders(user.getFuid(),ostate,keyword);
        PageInfo pageInfo = new PageInfo(ordersList);
        return pageInfo;
    }

    /**
     * 支付的方法
     * @return
     */
    @RequestMapping("/pay")
    public DefaultMsg pay(String oid, String oname, String money, String desc) {
        //先去查询该订单是否支付成功
        DefaultMsg defaultMsg = PayUtil.payQuery(oid);
        //支付成功
        if (defaultMsg.getSuccess() == 1) {
            //更改订单的付款状态
            defaultMsg.setSuccess(0);
            defaultMsg.setMsg("该订单已经支付");
            fuserService.updateOrderStates(Long.valueOf(oid));
        } else {
            defaultMsg = PayUtil.pay(oid, money, oname, desc);
        }
        return defaultMsg;
    }

    /**
     * 看订单是否支付成功 如果成功 改变订单状态
     * @return
     */
    @RequestMapping("/payCheck")
    public DefaultMsg paySuccess(String oid) throws Exception {
        DefaultMsg defaultMsg = PayUtil.payQuery(oid);
        //更改订单的付款状态
        if (defaultMsg.getSuccess() == 1) {
            System.out.println("订单支付成功");
            //获取订单号 更改支付状态
            System.out.println("oid:" + oid);
            DefaultMsg defaultMsg1 = fuserService.updateOrderStates(Long.valueOf(oid));
        } else {
            defaultMsg.setMsg("订单未支付成功");
            defaultMsg.setSuccess(0);
        }
        return defaultMsg;
    }

    /**
     * 查询我的地址
     * @param authentication
     * @return
     */
    @RequestMapping("/listMyAddress")
    public List<Address> listAllAddress(Authentication authentication) {
        Fuser fuser = (Fuser) this.getUser(authentication);
        List<Address> addresses = fuserService.listAllAddress(fuser.getFuid());
        return addresses;
    }

    @RequestMapping("/findByMoren")
    public Address findByMoren(Authentication authentication,@RequestParam(value = "uid") Long uid) {
        Fuser fuser = (Fuser) this.getUser(authentication);
        if (uid == null){
            uid = fuser.getFuid();
        }
        return fuserService.findByMoren(uid);
    }

    /**
     * 新增或修改地址
     * @param address
     * @return
     */
    @RequestMapping("/saveOrUpdateAddress")
    public DefaultMsg saveOrUpdateAddress(Address address, Authentication authentication) {
        Fuser fuser = (Fuser) this.getUser(authentication);
        address.setUid(fuser.getFuid());
        DefaultMsg defaultMsg = fuserService.saveOrUpdateAddress(address);
        return defaultMsg;
    }

    /**
     * 删除地址
     * @param aid
     * @param authentication
     * @return
     */
    @RequestMapping("/deleteAddress")
    public DefaultMsg deleteAddress(Long aid, Authentication authentication) {
        Fuser fuser = (Fuser) this.getUser(authentication);
        DefaultMsg defaultMsg = fuserService.deleteAddress(fuser.getFuid(), aid);
        return defaultMsg;

    }

    /**
     * 更改默认地址
     * @param address 选中的地址信息
     * @param authentication  校验用户
     * @return
     */
    @RequestMapping("/updateDefault")
    public DefaultMsg updateDefault(Address address, Authentication authentication) {
        Fuser fuser = (Fuser) this.getUser(authentication);
        return fuserService.updateDefault(address, fuser.getFuid());
    }

    /**
     * 上传头像
     * @param file
     * @return
     */
    @RequestMapping("/upload")
    public DefaultMsg upload(MultipartFile file) {
        String saveName = UUID.randomUUID().toString();
        File saveFile = new File("D:/upload/" + saveName);
        DefaultMsg defaultMsg = new DefaultMsg();
        try {
            file.transferTo(saveFile);
            defaultMsg.setTarget(saveName);
        } catch (IOException e) {
            defaultMsg.setMsg("上传文件失败");
            e.printStackTrace();
        }
        return defaultMsg;
    }

    /**
     * 修改订单状态 已收货
     * @param oid
     * @return
     */
    @RequestMapping("/updateOrdersSh")
    public DefaultMsg updateOrdersStateSh(Long oid) {
        DefaultMsg defaultMsg = fuserService.updateOrderStatesSh(oid);
        return defaultMsg;
    }

}
