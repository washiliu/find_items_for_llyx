package com.aaa.util;

/**
 * 封装前端调用后端的结果
 */
public class DefaultMsg {

    //状态码 200 请求成功 401 没有权限 403 请求没有权限
    private Integer code =200;
    //处理是否成功 成功 1 失败 0
    private Integer success=1;
    //处理失败 返回给前端的错误信息
    private String msg;
    //需要返回给前台的数据
    private Object target;
    //无参 构造 请求成功
    public DefaultMsg(){
        this(200,1,null,null);
    }
    //请求失败
    public DefaultMsg(String msg){
        this(200,0,msg,null);
    }
    //请求成功 需要给前端返回数据
    public DefaultMsg(Object target){
        this(200,1,null,target);
    }
    //全参构造方法
    public DefaultMsg(Integer code, Integer success, String msg, Object target){
        this.code=code;
        this.success=success;
        this.msg=msg;
        this.target=target;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }


    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public Object getTarget() {
        return target;
    }

    public void setTarget(Object target) {
        this.target = target;
    }
}
