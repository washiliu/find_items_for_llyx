package com.aaa.util;

import com.aaa.config.AlipayConfig;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.alipay.api.request.AlipayTradeQueryRequest;
import com.alipay.api.response.AlipayTradeQueryResponse;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * 支付宝支付封装的代码
 */
public class PayUtil {

    public static   AlipayClient alipayClient = new DefaultAlipayClient(AlipayConfig.gatewayUrl, AlipayConfig.app_id, AlipayConfig.merchant_private_key, "json", AlipayConfig.charset, AlipayConfig.alipay_public_key, AlipayConfig.sign_type);


    /**
     * 判断调用该请求的来源是否来自支付宝
     * @param request
     * @return
     * @throws Exception
     */
    public static DefaultMsg paySuccess(HttpServletRequest request) throws Exception{
        DefaultMsg defaultMsg = new DefaultMsg();
        //获取到支付成功的订单id  更改订单id的状态
        Map<String,String> params = new HashMap<String,String>();
        Map<String,String[]> requestParams = request.getParameterMap();
        for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext();) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i]
                        : valueStr + values[i] + ",";
            }
            //乱码解决，这段代码在出现乱码时使用
            valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
            params.put(name, valueStr);
        }

        boolean signVerified = AlipaySignature.rsaCheckV1(params, AlipayConfig.alipay_public_key, AlipayConfig.charset, AlipayConfig.sign_type); //调用SDK验证签名

        //——请在这里编写您的程序（以下代码仅作参考）——
        if(signVerified) {
            //商户订单号
            String out_trade_no = new String(request.getParameter("out_trade_no").getBytes("ISO-8859-1"),"UTF-8");

            //支付宝交易号
            String trade_no = new String(request.getParameter("trade_no").getBytes("ISO-8859-1"),"UTF-8");

            //付款金额
            String total_amount = new String(request.getParameter("total_amount").getBytes("ISO-8859-1"),"UTF-8");
            System.out.println("支付成功的订单号是:"+out_trade_no);
            System.out.println("付款金额是:"+total_amount);
            defaultMsg.setTarget(out_trade_no);

        }else {
            defaultMsg.setSuccess(0);
            defaultMsg.setMsg("非法请求");
        }
        return  defaultMsg;
    }
    /**
     * 支付宝支付的代码
     * @return
     */
    public static DefaultMsg pay(String out_trade_no, String total_amount, String subject, String body){

        //设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(AlipayConfig.return_url);
        alipayRequest.setNotifyUrl(AlipayConfig.notify_url);

//        //商户订单号，商户网站订单系统中唯一订单号，必填
//        String out_trade_no = oid;
//        //付款金额，必填
//        String total_amount =money;
//        //订单名称，必填
//        String subject = oname;
//        //商品描述，可空
//        String body = desc;

        alipayRequest.setBizContent("{\"out_trade_no\":\""+ out_trade_no +"\","
                + "\"total_amount\":\""+ total_amount +"\","
                + "\"subject\":\""+ subject +"\","
                + "\"body\":\""+ body +"\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");

        //若想给BizContent增加其他可选请求参数，以增加自定义超时时间参数timeout_express来举例说明
        //alipayRequest.setBizContent("{\"out_trade_no\":\""+ out_trade_no +"\","
        //		+ "\"total_amount\":\""+ total_amount +"\","
        //		+ "\"subject\":\""+ subject +"\","
        //		+ "\"body\":\""+ body +"\","
        //		+ "\"timeout_express\":\"10m\","
        //		+ "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");
        //请求参数可查阅【电脑网站支付的API文档-alipay.trade.page.pay-请求参数】章节

        //请求
        String result = null;
        DefaultMsg defaultMsg = new DefaultMsg() ;
        try {
            result = alipayClient.pageExecute(alipayRequest).getBody();
        } catch (AlipayApiException e) {
            e.printStackTrace();
            defaultMsg.setSuccess(0);
            defaultMsg.setMsg(e.getMessage());
            return defaultMsg;
        }

        //request.getRequestDispatcher("test.jsp").forward(request,response);
        defaultMsg.setTarget(result);
        return defaultMsg;
    }

    /**
     * 查询订单是否付款成功的请求
     * @param out_trade_no
     * @return
     */
    public static DefaultMsg payQuery(String out_trade_no){
        DefaultMsg defaultMsg = new DefaultMsg();
        AlipayTradeQueryRequest request = new AlipayTradeQueryRequest();
        request.setBizContent("{" +
                "\"out_trade_no\":\""+out_trade_no+"\"," +
                "\"trade_no\":\"\""+
                "  }");
        try {
            AlipayTradeQueryResponse response = alipayClient.execute(request);
            System.out.println(response);
            String tradeStatus = response.getTradeStatus();

            if(response.isSuccess()){
                System.out.println("调用成功");
                if(tradeStatus.equals("TRADE_SUCCESS")){
                    return  defaultMsg;
                }
            } else {
                defaultMsg.setSuccess(0);
                defaultMsg.setMsg("调用失败");
            }
        }catch (Exception e){
            defaultMsg.setSuccess(0);
            defaultMsg.setMsg(e.getMessage());
        }
        return defaultMsg;


    }
}
