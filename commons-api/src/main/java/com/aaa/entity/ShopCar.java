package com.aaa.entity;
import lombok.Data;

//用于接受购物车信息
@Data
public class ShopCar {
    private Product product;
    private Fuser fuser;
    private Long num;
    private Long sid;
    private String  price;
}
