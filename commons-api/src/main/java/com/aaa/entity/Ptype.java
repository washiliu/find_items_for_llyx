package com.aaa.entity;

import lombok.Data;

@Data
public class Ptype {
    private Long tid;
    private String tname;
}
