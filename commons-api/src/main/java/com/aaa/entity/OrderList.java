package com.aaa.entity;

import lombok.Data;

/*
* 订单详情
* */
@Data
public class OrderList {
    private Long olid;
    private Long oid;
    private Long pid;
    private Long count;
    private String price;
    private  Product product;
}
