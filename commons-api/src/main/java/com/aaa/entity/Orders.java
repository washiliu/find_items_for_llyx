package com.aaa.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
/**
 * 订单实体类
 */
@Data
public class Orders implements Serializable {
    private Long oid;
    private Long ouid;
    private Long ostate;
    private String cprice;
    private String createDate;
    private List<OrderList> orderList;
    private String address;
    private Fuser fuser;
    private String phone;
    private String person;
}
