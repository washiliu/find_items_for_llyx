package com.aaa.entity;

import lombok.Data;

@Data
public class ProDisCount {
    private Long sid;
    private Long pid;
    private String dstate;
    private String nums;
    private String discount;
    private String startTime;
    private String stopTime;
    private Product product;
}
