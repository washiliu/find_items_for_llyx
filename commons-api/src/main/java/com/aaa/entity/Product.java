package com.aaa.entity;

import lombok.Data;

@Data
public class Product {
    private Long pid;

    private String pname;

    private Double price;

    private String imgPath;
    private Long tid;
    private Long state;
    private Long stock;
    private Ptype ptype;
    private Long  sid;
    private ProDisCount proDisCount;
}