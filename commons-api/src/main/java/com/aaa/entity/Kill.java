package com.aaa.entity;

import lombok.Data;

/**
 * 秒杀商品订单
 */
@Data
public class Kill {
    public Integer fid;
    public Integer sid;
    public String verson;
}
