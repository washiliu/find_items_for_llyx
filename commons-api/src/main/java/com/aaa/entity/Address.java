package com.aaa.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Address implements Serializable {
    /**
     * 主键
     */
    private Long aid;

    /**
     * 收货人
     */
    private String person;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 详细地址
     */
    private String addr;

    /**
     * 用户id
     */
    private Long uid;

    /**
     *是否是默认地址
     */
    private Long isDefault;

    /**
     * 省市县
     */
    private String city;
}
