package com.aaa.config;
import java.io.FileWriter;
import java.io.IOException;

/* *
 *类名：AlipayConfig
 *功能：基础配置类
 *详细：设置帐户有关信息及返回路径
 *修改日期：2022-08-10
 *说明：
 *以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 *该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
 */

public class AlipayConfig {

    // ↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
    public static String app_id = "2021000119648661";
    // 商户私钥，您的PKCS8格式RSA2私钥
    public static String merchant_private_key = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC9yqnogWzuP8DnGemTMReYJaQB1oWXx5l2dtKWebKVasNLdErwpHHvWMAb6Nu8q3O4+Usz7+u5/lP2qPid52KhFN8C1urHMnBorJd0QVUewBUIEe/cwgMmckQrDZt8dwZmSQReYFaenpvGmkOAL/DZlfcxtkhamw42MPjUFmfXEWZw9iTBDSo6WC4udyjIebKtUk8cI0q55kCWu1d+96nkqKdx+AG+l+b+QyVaImKtUTMPjG2caPFItu2YTc1G463IJGRDXOxNRkHxeUujTCWEGSlDUsxZ2YybbFdKZ/athBsvSLHSSui36lI3HXjdhd+rmKti3uD+cflHAbNvkftJAgMBAAECggEBALGXTd2uHcviBBM7gACnUzifIBRIM8GOl97t2KfNQHnn6M5WBzmd5q7cEQcMoXCtzh8rxl7XBXCItvB7tAetV/D6oqL5j77NR7nqpd22tHuNqSBgCF49fM0g5wCSPwf8KBTWBix5YRQTvhfciYUNVwWC+keeFZ4SsP2sXWm3fXsTwcBRXfaRbp3psRpKp+YgCw320nESJfXZP3XMUwi/o8Swe16s2U6x9Vp0F/YIKPnjJwG0vC5VHQiIA963dY/peHvE5OkOIOFEIDuTZVbCFCrV5Um958Q7OwTFidfEU7rp5+ttZPzRJ2ozpkgWT6yxUUO0aa6QrmIMPWb1P8xhfAECgYEA97RgP6BcH5HFXJhd23y5SXkUCfwBnWxPuVGdOx22zz0glZk1vC9MlVGCK/f1rhk2D1Hp/nw2TeoI1zHgohsNRUR+ZbzOCsbbz9uzvH8Cn+ZljLhuZ1OMwB7CeXgmVV0jat6HLQ+ExPPZ5dkZneXlsbBeXBMft55SGS+LCmmq3CkCgYEAxCXJruz2jR/MHUM6CbcN74yiuYMqg6yrusmzwbAB7uQpX79PVo922d0ShJ7wWHZ3V+K45ufi1xPIxQBvygHP7X5VJ8ugOOjDkwglxTOVzcPddOcNiF2iv1y3ssjc6KKXv18k8BqynlmZsnek8Z079zC4xrDv4gvN7ESat4KiCiECgYBtEX5j5b9Ko0wM1oZVU9jeMjMRpmYro/qCth4OXqoVHGxRktGnWAhXozk3FQqXq0AzhLUMujLLIebconivyEuSmXMyZu0ur5swLiQ5No+4SqGiUb9DPfW2zpSBGdd/NPCYaP2KaM/kTR8/5tUxwn3ePLN/xWfs2Qh7UITfg+GY6QKBgQC0eUBUnttLhhI8anqgLbm4y2F9513awalQt/2ZHOTqdSxMWK4bKu1kD7DWLZdfDcFJL+aJLprc4es8t+Yjq/8wuaiIcSb+tcwdIMX/6DLRnIHK6jzdV6pDAIBVE9OOiOuuvVAzS51FbZOpTcSKOu3Nxxp8VpihF9NkrMreRmfpgQKBgBnN9gRAFhKin4L6RGj39p1fxUNX8R9kRoK6OXEyFCH7MJmQvhlmxJmSbH0ANA+fbdN4AyHXqmnP3J8/xaOLTCnKrLuFwmwcUZFgg/tU9Tkj9TfTnxfGe9iL7T0HoJu5FZLyqRWKBDERINJKm3b1aNT4vDIsQMCsKVHTnTSwRx2c";
    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm
    // 对应APPID下的支付宝公钥。
    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlS9UbnxK0LaBFoRnPFpSYXLizahTMLvvWlJEGvUif4gcB5XWJKJi2PHZFN/D5yxhpeiRNwykYzX1IT7+5hKihNVt00c1A+SktEFui2BXl9mprUzAyI8ultToWiwHRQDbigY1M0EizNfzIv/ZS7R46FMAj4HdJ3FeTnu+M6XviD1iERRRIZr/PFmRTL6cKyLeM68f+TfEGRXPafAWgI/HhQXXiNBiKeQMzlbK5s2epW9mAIP99zHgQ7Ihpzyc/0kFiXX14+6hk+5FziKv3hejTTQ/3B10cLPrq20PJV/3ppWJC6qABQL/+37dn+2mg77nDEcbMozf1qmlTGkXdRjCfQIDAQAB";
    // 服务器异步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    /**
     * 返回的时候此页面不会返回到用户页面，只会执行你写到控制器里的地址
     */

    // 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String notify_url = "http://工程公网访问地址/alipay.trade.page.pay-JAVA-UTF-8/notify_url.jsp";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String return_url = "http://localhost:9000/#/PaySuccess";

    // 签名方式
    public static String sign_type = "RSA2";

    // 字符编码格式
    public static String charset = "utf-8";

    // 支付宝网关
    public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    // 支付宝网关
    public static String log_path = "C:\\";


//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    /**
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

