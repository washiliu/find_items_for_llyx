package com.aaa.controller;

import com.aaa.entity.ProDisCount;
import com.aaa.entity.Product;
import com.aaa.entity.Ptype;
import com.aaa.service.ProductService;
import com.aaa.util.DefaultMsg;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/pro")
public class ProductController {
    @Resource
    private ProductService productService;

    @PostMapping("/listData")
    public PageInfo listAll(@RequestParam(defaultValue = "") String keyword, Integer currentPage, Integer pageSize, Long tid) {
        PageHelper.startPage(currentPage, pageSize);
        List<Product> products = productService.listAll(keyword, tid);
        PageInfo pageInfo = new PageInfo(products);
        return pageInfo;
    }

    //根据id查询
    @RequestMapping("/findByPid")
    public Product findByPid(@RequestParam Long pid) {
        Product product = productService.selectByPrimaryKey(pid);
        return product;
    }

    //下拉框
    @RequestMapping("/listPtype")
    public List<Ptype> listPtype() {
        return productService.listPtype();
    }

    @RequestMapping("/update")
    public Integer update(@RequestBody Product product) {
        return productService.updateStock(product);
    }

    @RequestMapping("/searchPro")
    public DefaultMsg searchPro() {
        DefaultMsg defaultMsg = productService.selectByPid();

        return defaultMsg;
    }

    //查询单个商品的优惠信息
    @RequestMapping("/findProDisCount")
    public DefaultMsg findProDisCount(@RequestParam String pids) {
        DefaultMsg defaultMsg = productService.selectByPids(pids);
        return defaultMsg;
    }
}
