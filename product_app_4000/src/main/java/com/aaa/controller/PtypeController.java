package com.aaa.controller;

import com.aaa.entity.Ptype;
import com.aaa.service.PtypeService;
import com.aaa.util.DefaultMsg;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/ptype")
public class PtypeController {
    @Resource
    private PtypeService ptypeService;
    @RequestMapping("/listAll")
    public PageInfo listAll(String keyword){
        PageHelper.startPage(1,5);
        List<Ptype> ptypes = ptypeService.listAll(keyword);
        PageInfo pageInfo = new PageInfo(ptypes);
        return pageInfo;
    }
    @RequestMapping("/saveOrUpdate")
    public DefaultMsg saveOrUpdate(Ptype ptype){
        return ptypeService.saveOrUpdate(ptype);
    }
    @RequestMapping("/delete")
    public DefaultMsg delete(Long tid){
        System.out.println("tid:"+tid);
        return   ptypeService.delete(tid);
    }

}
