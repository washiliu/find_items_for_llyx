package com.aaa.service.impl;

import com.aaa.dao.PtypeDao;
import com.aaa.entity.Ptype;
import com.aaa.service.PtypeService;
import com.aaa.util.DefaultMsg;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class PtypeServiceImpl implements PtypeService {
    @Resource
    private PtypeDao ptypeDao;

    //列表
    @Override
    public List<Ptype> listAll(String keyword) {
        return ptypeDao.selectList(new QueryWrapper<Ptype>().like(keyword.length() > 0, "tname", keyword));
    }

    //删除
    @Override
    public DefaultMsg delete(Long tid) {
        DefaultMsg defaultMsg = new DefaultMsg();
        int i = ptypeDao.delete(new QueryWrapper<Ptype>().eq("tid", tid));
        if (i == 0) {
            defaultMsg.setSuccess(0);
            defaultMsg.setMsg("删除失败");
        }
        return defaultMsg;
    }

    //保存或修改
    @Override
    public DefaultMsg saveOrUpdate(Ptype ptype) {
        Long tid = ptype.getTid();
        Integer count = 0;
        if (tid == null) {
            count = ptypeDao.insert(ptype);
        } else {
            count = ptypeDao.update(ptype, new UpdateWrapper<Ptype>().eq("tid", ptype.getTid()));

        }
        DefaultMsg defaultMsg = new DefaultMsg();
        if (count == 0) {
            defaultMsg.setMsg("操作失败");
            defaultMsg.setSuccess(0);
        }
        return defaultMsg;
    }

}
