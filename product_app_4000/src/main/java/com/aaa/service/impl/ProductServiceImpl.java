package com.aaa.service.impl;

import com.aaa.dao.ProductDao;
import com.aaa.entity.ProDisCount;
import com.aaa.entity.Product;
import com.aaa.entity.Ptype;
import com.aaa.service.ProductService;
import com.aaa.util.DefaultMsg;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Resource
    private ProductDao productDao;

    @Override
    public Product selectByPrimaryKey(Long pid) {
        return productDao.selectByPrimaryKey(pid);
    }

    @Override
    public List<Product> listAll(String keyword, Long tid) {
        return productDao.listAll(keyword, tid);
    }

    @Override
    public List<Ptype> listPtype() {
        return productDao.listPtype();
    }

    @Override
    public Integer updateStock(Product product) {
        //将 product 中的 sid设为空值
        return productDao.updateStock(product);
    }

    @Override
    public DefaultMsg selectByPid() {
        Integer integer = productDao.selectByPid();
        DefaultMsg defaultMsg = new DefaultMsg();
        if (integer==null){
            defaultMsg.setSuccess(0);
            defaultMsg.setMsg("没有优惠商品");
        }else {
            defaultMsg.setSuccess(1);
            defaultMsg.setMsg("有优惠商品");
            List<Product> products = productDao.listDiscount();
            defaultMsg.setTarget(products);

            }

        return defaultMsg;
    }

    @Override
    public DefaultMsg selectByPids(String pids) {
        DefaultMsg defaultMsg = new DefaultMsg();
        //根据逗号拆分pids
        String[] split = pids.split(",");
        //遍历pids
        List<ProDisCount> proDisCounts = new ArrayList<>();
        for (String s : split) {
            //查询每个商品的优惠信息
            ProDisCount proDisCount = productDao.selectByPids(Long.parseLong(s));
            proDisCounts.add(proDisCount);
        }
        defaultMsg.setTarget(proDisCounts);
        return defaultMsg;
    }

}
