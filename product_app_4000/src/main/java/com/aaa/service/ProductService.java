package com.aaa.service;

import com.aaa.entity.Product;
import com.aaa.entity.Ptype;
import com.aaa.util.DefaultMsg;

import java.util.List;

public interface ProductService {
    Product selectByPrimaryKey(Long pid);
    List<Product> listAll(String keyword,Long tid);
    List<Ptype> listPtype();
    Integer updateStock(Product product);
    //查询优惠商品
    DefaultMsg selectByPid();
    //查询优惠的商品的信息
    DefaultMsg selectByPids(String pids);
}
