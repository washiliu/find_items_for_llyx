package com.aaa.service;

import com.aaa.entity.Ptype;
import com.aaa.util.DefaultMsg;

import java.util.List;

public interface PtypeService {


    List<Ptype> listAll(String keyword);
    DefaultMsg delete(Long tid);
    DefaultMsg saveOrUpdate(Ptype ptype);

}
