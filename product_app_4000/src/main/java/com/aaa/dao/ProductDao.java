package com.aaa.dao;

import com.aaa.entity.ProDisCount;
import com.aaa.entity.Product;
import com.aaa.entity.Ptype;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface ProductDao extends BaseMapper<Product> {

    //根据id查询
    Product selectByPrimaryKey(Long pid);
    //查询
    List<Product> listAll(@Param("keyword") String keyword,@Param("tid") Long tid);
    //下拉框数据
    List<Ptype> listPtype();
    //修改库存
    Integer updateStock(Product product);
    //查询是否存在优惠商品
    Integer selectByPid();
    //查询优惠的商品
    List<Product> listDiscount();
    //查询单个优惠商品的信息
    ProDisCount selectByPids(Long pid);
}