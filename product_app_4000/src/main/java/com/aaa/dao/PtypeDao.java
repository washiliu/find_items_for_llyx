package com.aaa.dao;

import com.aaa.entity.Ptype;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;


/*
* 商品类别管理
* */
public interface PtypeDao extends BaseMapper<Ptype> {

}
