package com.aaa;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@MapperScan("com.aaa.dao")
@EnableCaching
@EnableDiscoveryClient
public class ProductApp4000 {
    public static void main(String[] args) {
        SpringApplication.run(ProductApp4000.class, args);
    }
}
