package com.aaa.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/*
 * rabbitmq配置类
 * */
@Configuration
public class RabbitConfig {
    /**
     * 在容器中声明名称为dlOrderQueue的死信队列队列 原先rabbitMq中没有该队列也没关系，
     * spring boot 会自动在第一次使用该队列的时候帮我们在rabbitMq中创建该队列  创建交换机也是这个道理
     *
     * @return
     */
    @Bean
    public Queue dlOrderQueue() {
        //设置队列的其他参数
        Map<String, Object> map = new HashMap();
        return new Queue("dlOrderQueue", true, false, false, map);
    }

    /**
     * 创建死信交换机
     *
     * @return
     */
    @Bean
    public FanoutExchange dlOrderExchange() {
        //设置创建交换机参数
        Map<String, Object> map = new HashMap();
        return new FanoutExchange("dlOrderExchange", true, true, map);
    }

    /**
     * 那死信列队绑定到死信交换机上
     */
    @Bean
    public Binding bindDlOrdeQueueTobindDlOrderExchange(Queue dlOrderQueue, FanoutExchange dlOrderExchange) {
        return BindingBuilder.bind(dlOrderQueue).to(dlOrderExchange);
    }

    /**
     * 创建普通队列 给普通队列设置信息过期时间 超过时间吧信息放到死信交换机里
     */
    @Bean
    public Queue normalOrderQueue() {
        Map<String, Object> map = new HashMap();
        //设置过期时间  20s
        map.put("x-message-ttl",60000);
        //设置过期消息发送到死信交换机
        map.put("x-dead-letter-exchange", "dlOrderExchange");
        return new Queue("normalOrderQueue", true, false, false, map);
    }

    /**
     * 创建普通交换机
     */
    @Bean
    public FanoutExchange normalOrderExchange() {
        //设置创建交换机参数
        Map<String, Object> map = new HashMap();
        return new FanoutExchange("normalOrderExchange", true, true, map);
    }
    /**
     * 吧普通队列帮订到普通交换机上
     */
    @Bean
    public Binding bindNormalQueueToNormalExchange(Queue normalOrderQueue, FanoutExchange normalOrderExchange) {
        return BindingBuilder.bind(normalOrderQueue).to(normalOrderExchange);
    }
}
