package com.aaa.listener;

import com.aaa.entity.Orders;
import com.aaa.service.FeginProductServie;
import com.aaa.service.OrderService;
import com.aaa.util.DefaultMsg;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * rabbitmq 监听类
 */
@Component
@Transactional
public class RabbitMqListener {
    @Autowired
    private FeginProductServie feginProductServie;

    @Autowired
    private OrderService orderService;
    /**
     * 监听死信dlOrderQueue队列   如果这个队列有数据 每时每刻都生效
     * @param orders
     * @param message
     * @param channel
     */
    @RabbitListener(queues = "dlOrderQueue")
    public void readFromBootUserObj(Orders orders, Message message, Channel channel){
//        //因为无论怎么 消息都会进入这个死信  消息队列  所以在进入之前查一下付款没
//        Orders byId = orderService.findByOid(orders.getOid());
//        //如果订单状态还是 待付款
//        if (byId.getOstate() == 1) {
//            //删除订单  (理论上应该修改为 “已取消”  )
//            orderService.deleteMyOrder(orders.getOuid(), orders.getOid());
//            // 查询订单详情 并删除库存
//            List<OrderList> orderItems = byId.getOrderList();
//            //循环每个商品
//            for (OrderList orderItem : orderItems) {
//                //查询此商品的库存
//                Product product = feginProductServie.findByPid(orderItem.getPid());
//                //修改商品库存  把商品数量加回来
//                product.setStock(product.getStock() + orderItem.getCount());
//                Integer update = feginProductServie.update(product);
//                if (update == 0 || update == null) {
//                    throw new RuntimeException();
//                }
//            }
//        }
        //根据订单id查询订单状态
        Orders byOid = orderService.findByOid(orders.getOid());
        if (byOid.getOstate()==0){
            DefaultMsg defaultMsg = orderService.deleteMyOrder(orders.getOuid(),orders.getOid());
        }
        System.out.println("接收到消息(订单id):"+orders.getOid());
        System.out.println("接收到消息(用户id):"+orders.getOuid());
        System.out.println("消息ID："+message.getMessageProperties().getMessageId());
        System.out.println("接收到消息(订单id):"+orders.getOid());
        System.out.println("消息ID："+message.getMessageProperties().getMessageId());
    }
}
