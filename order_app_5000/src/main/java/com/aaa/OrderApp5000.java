package com.aaa;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author BC
 */
@SpringBootApplication
@MapperScan("com.aaa.dao")
@EnableCaching
@EnableDiscoveryClient
@EnableFeignClients
public class OrderApp5000 {
    public static void main(String[] args) {
        SpringApplication.run(OrderApp5000.class, args);
    }
}

