package com.aaa.dao;

import com.aaa.entity.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrederDao {
    //查询所有订单
    List<Orders> findAll();
    //生成订单
    Integer saveOrders(Orders orders);

    //生成订单详情
    Integer saveOrdersList(OrderList orderList);

    //查询我的订单
    List<Orders> listOrders(@Param("uid") Long uid, @Param("ostate") Long ostate);

    //根据订单id查询订单详情
    List<OrderList> listOrdersList(Long oid);

    //根据id查询订单
    Orders ByOid(@Param("oid") Long oid,@Param("ostate") Long ostate);

    //根据关键字查询我的订单详情
    List<OrderList> listOredrByKeword(@Param("uid") Long uid, @Param("keyword") String keyword);

    //修改订单状态
    Integer updateOrderState(@Param("oid") Long oid, @Param("state") Long state);

    //删除我的订单
    Integer deleteMyOrder(@Param("uid") Long uid, @Param("oid") Long oid);

    //根据订单id查询订单状态
    Orders findByOid(Long oid);
    Fuser findByFuname(String username);

}
