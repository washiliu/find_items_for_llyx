package com.aaa.dao.controller;

import com.aaa.entity.*;
import com.aaa.service.OrderService;
import com.aaa.util.DefaultMsg;
import com.aaa.util.PayUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/pay")
public class OrderController {
    @Resource
    private OrderService orderService;

    public Object getUser(Authentication authentication) {
        Object principal = authentication.getPrincipal();
        Fuser byFuname = orderService.findByFuname(principal.toString());
        return byFuname;
    }

    /**
     * 生成订单和订单详情
     *
     * @param
     * @return
     */
    @RequestMapping("/saveorders")
    public DefaultMsg saveorders(String abc, String pids, Address address, Authentication authentication,String pid) {
//        Fuser user = (Fuser) this.getUser(authentication);
//        return orderService.saveorders(abc, pids, user.getFuid(), address);
        return null;
    }

    /**
     * 查询我的订单
     *
     * @param authentication
     * @return
     */
    @RequestMapping("/listMyOrders")
    public PageInfo listOrders(Authentication authentication, Long ostate, Integer currentPage, Integer pageSize, String keyword) {
        Fuser user = (Fuser) this.getUser(authentication);
        PageHelper.startPage(currentPage, pageSize);
        List<Orders> ordersList = orderService.listOrders(user.getFuid(), ostate, keyword);
        PageInfo pageInfo = new PageInfo(ordersList);
        return pageInfo;
    }

    /**
     * 支付的方法
     *
     * @return
     */
    @RequestMapping("/pays")
    public DefaultMsg pay(String oid, String oname, String money, String desc) {
        //先去查询该订单是否支付成功
        DefaultMsg defaultMsg = PayUtil.payQuery(oid);
        //支付成功
        if (defaultMsg.getSuccess() == 1) {
            //更改订单的付款状态
            defaultMsg.setSuccess(0);
            defaultMsg.setMsg("该订单已经支付");
            orderService.updateOrderStates(Long.valueOf(oid));
        } else {
            defaultMsg = PayUtil.pay(oid, money, oname, desc);
        }
        return defaultMsg;
    }

    /**
     * 看订单是否支付成功 如果成功 改变订单状态
     *
     * @return
     */
    @RequestMapping("/payCheck")
    public DefaultMsg paySuccess(String oid) throws Exception {
        DefaultMsg defaultMsg = PayUtil.payQuery(oid);
        //更改订单的付款状态
        if (defaultMsg.getSuccess() == 1) {
            System.out.println("订单支付成功");
            //获取订单号 更改支付状态
            System.out.println("oid:" + oid);
            DefaultMsg defaultMsg1 = orderService.updateOrderStates(Long.valueOf(oid));
        } else {
            defaultMsg.setMsg("订单未支付成功");
            defaultMsg.setSuccess(0);
        }
        return defaultMsg;
    }

    /**
     * 修改订单状态 已收货
     *
     * @param oid
     * @return
     */
    @RequestMapping("/updateOrdersSh")
    public DefaultMsg updateOrdersStateSh(Long oid) {
        DefaultMsg defaultMsg = orderService.updateOrderStatesSh(oid);
        return defaultMsg;
    }

}
