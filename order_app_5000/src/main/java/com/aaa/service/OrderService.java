package com.aaa.service;

import com.aaa.entity.Address;
import com.aaa.entity.Fuser;
import com.aaa.entity.Orders;
import com.aaa.util.DefaultMsg;
import org.springframework.security.core.Authentication;

import java.util.List;

public interface OrderService {
    //生成订单和订单详情
     DefaultMsg saveorders(String abc, String pids,Long uid, Address address);
    //查看我的订单
    List<Orders> listOrders(Long uid,Long ostate,String keyword);
    //修改订单状态 待发货
    DefaultMsg updateOrderStates(Long oid);
    //修改订单状态 已收货
    DefaultMsg updateOrderStatesSh(Long oid);
    //删除我的订单
    DefaultMsg deleteMyOrder(Long uid ,Long oid);
    //根据订单id查询订单
    Orders findByOid(Long oid);
    //根据账号查询用户
    Fuser findByFuname(String username);

}
