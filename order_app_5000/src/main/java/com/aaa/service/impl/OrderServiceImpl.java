package com.aaa.service.impl;

import cn.hutool.core.date.DateUtil;
import com.aaa.dao.OrederDao;
import com.aaa.entity.*;
import com.aaa.service.FeginProductServie;
import com.aaa.service.OrderService;
import com.aaa.util.DefaultMsg;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Resource
    private OrederDao orederDao;
    @Autowired
    private FeginProductServie feginProductServie;

    /**
     * 生成订单
     *
     * @param abc  拼凑的地址信息
     * @param pids 拼凑的商品id
     * @param
     * @return
     */
    @Override
    public DefaultMsg saveorders(String abc, String pids, Long uid, Address address) {
        //获取当前时间
        //创建时间
        String createDate = DateUtil.now();
        //存储要生成的订单id
        Orders orders = new Orders();
        //组装订单详情数据集合
        List<OrderList> orderSList = new ArrayList<OrderList>();
        //商品的id集合
        String[] pidArray = pids.split(",");
        //声明订单总金额
        float money = 0;
        for (String pid : pidArray) {
            orders.setAddress(abc.substring(9, abc.length()));
            //根据商品id查出商品的信息
            Product product = feginProductServie.findByPid(Long.valueOf(pid));
            //根据商品id 查出购物车中 相对应商品的数量
            Object num1 = redisTemplate.opsForHash().get(uid, pid);
            /**
             * 商品详细数据
             */
            Long num = Long.valueOf(num1.toString());
            OrderList orderList = new OrderList();
            //对应商品的数量
            orderList.setCount(num);
            //对应商品的id
            orderList.setPid(product.getPid());
            //对应商品的单价
            orderList.setPrice(String.valueOf(product.getPrice()));
            orderSList.add(orderList);
            /**
             * 查询地址
             */
            Address byMoren = address;
            Integer price = Integer.valueOf(product.getPrice().toString().substring(0, product.getPrice().toString().length() - 2));
            if (product.getSid() != null) {
                //把"0.9"转为0.9
                Integer discount = Integer.parseInt(product.getProDisCount().getDiscount().substring(2, 3));
                float f = discount / 10f;
                money += num * price * f;
            } else {
                money += num * price;
            }
            orders.setPhone(byMoren.getPhone());
            orders.setPerson(byMoren.getPerson());
            orders.setCreateDate(createDate);
            orders.setOstate(Long.valueOf(0));
            orders.setOuid(uid);
        }
        orders.setCprice(String.valueOf(money));
        Integer integer = orederDao.saveOrders(orders);
        if (integer == 0) {
            throw new RuntimeException("保存订单失败");
        } else {
            rabbitTemplate.convertAndSend("normalOrderExchange", "", orders);
        }
        //保存订单详情
        Long oid = orders.getOid();
        for (OrderList list : orderSList) {
            list.setOid(oid);
            Integer integer1 = orederDao.saveOrdersList(list);
            if (integer1 == 0 || integer == null) {
                throw new RuntimeException("订单详情报错错误");
            }
        }
        //移除购物车中对应的信息
        for (String pid : pidArray) {
            Long delete = redisTemplate.opsForHash().delete(uid, pid);
            if (delete == 0) {
                throw new RuntimeException("移除购物车信息出错");
            }
        }
        //更改 减少库存
        for (OrderList orderItem : orderSList) {
            Product byPname = feginProductServie.findByPid(orderItem.getPid());
            Product product = new Product();
            product.setPid(orderItem.getPid());
            product.setStock(byPname.getStock() - orderItem.getCount());
            Integer update = feginProductServie.update(product);
            if (update == 0) {
                throw new RuntimeException("更改库存错误");
            }
        }
        DefaultMsg defaultMsg = new DefaultMsg();
        defaultMsg.setTarget(orders);
        return defaultMsg;
    }

    /**
     * 查看我的订单
     *
     * @param uid
     * @return
     */
    @Override
    public List<Orders> listOrders(Long uid, Long ostate, String keyword) {
        List<Orders> orders = new ArrayList<>();
        if (keyword != "") {
            //如果有关键字 先根据关键字 和用户id查询 包含商品 的订单详情
            List<OrderList> orderLists = orederDao.listOredrByKeword(uid, keyword);
            for (OrderList orderList : orderLists) {
                //拿着订单详情的 用订单id查询订单
                Orders orders1 = orederDao.ByOid(orderList.getOid(), ostate);
                orders.add(orders1);
            }
        } else {
            //没有关键字 直接根据所有订单
            orders = orederDao.listOrders(uid, ostate);
        }
        //移除 数组中的空值
        orders.removeAll(Collections.singleton(null));
        //如果 数组为空 则返回空
        if (CollectionUtils.isNotEmpty(orders)) {
            //遍历所有查出的所有订单 并查出 对应的订单详情
            for (Orders orders1 : orders) {
                List<OrderList> orderLists = orederDao.listOrdersList(orders1.getOid());
                orders1.setOrderList(orderLists);
            }
        }
        return orders;
    }

    /**
     * 修改订单状态待发货
     *
     * @param oid
     * @param
     * @return
     */
    @Override
    public DefaultMsg updateOrderStates(Long oid) {
        DefaultMsg msg = new DefaultMsg();
        Integer integer = orederDao.updateOrderState(oid, Long.valueOf(1));
        if (integer == null) {
            msg.setMsg("失败");
            msg.setSuccess(0);
        }
        return msg;
    }

    /**
     * 修改订单状态已收货
     *
     * @param oid
     * @return
     */
    @Override
    public DefaultMsg updateOrderStatesSh(Long oid) {
        DefaultMsg msg = new DefaultMsg();
        Integer integer = orederDao.updateOrderState(oid, Long.valueOf(3));
        if (integer == null) {
            msg.setMsg("失败");
            msg.setSuccess(0);
        }
        return msg;
    }

    /**
     * 删除我的订单
     *
     * @param oid
     * @param uid
     * @return
     */
    @Override
    public DefaultMsg deleteMyOrder(Long uid, Long oid) {
        Integer integer = orederDao.deleteMyOrder(uid, oid);
        DefaultMsg defaultMsg = new DefaultMsg();
        if (integer == 0) {
            defaultMsg.setMsg("删除失败");
            defaultMsg.setSuccess(0);
        }
        return defaultMsg;
    }

    /**
     * 根据订单id查询订单
     *
     * @param oid
     * @return
     */
    @Override
    public Orders findByOid(Long oid) {
        return orederDao.findByOid(oid);
    }

    @Override
    public Fuser findByFuname(String username) {
        return orederDao.findByFuname(username);
    }

    /*//@PostConstruct该注解被用来修饰一个非静态的void（）方法。
    // 被@PostConstruct修饰的方法会在服务器加载Servlet的时候运行，并且只会被服务器执行一次。
    // PostConstruct在构造函数之后执行，init（）方法之前执行。
    @PostConstruct
    public void init() {
        //是一个保留的关键字 可以声明成员变量方法类以及本地变量
        final OrderServiceImpl that = this;
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(1000);
                        that.timerCancel();
                    } catch (Exception e) {

                    }
                }
            }
        }).start();
    }

    *//**
     * 每1秒执行一次
     * 扫描超时的进行中的订单，自动更新到取消状态
     *//*
    public void timerCancel() {
        List<Orders> orderForms = orederDao.findAll();
        for (int i = 0; i < orderForms.size(); i++) {
            String buy_seats = orderForms.get(i).getBuy_seats();
            Long tid = orderForms.get(i).getTicketSales().getTid();
            System.out.println("tid" + tid);
            String[] split = buy_seats.split(",");
            Long length = Long.valueOf(split.length);
            System.out.println(length + "length");
            orderFormDao.outUpdate(length, tid);
        }
        orderFormDao.updateStateCancel();

    }*/
}
