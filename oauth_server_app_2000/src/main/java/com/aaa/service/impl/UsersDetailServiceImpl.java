package com.aaa.service.impl;

import com.aaa.dao.UserDao;
import com.aaa.entity.Fuser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.SimpleTimeZone;

@Component
public class UsersDetailServiceImpl implements UserDetailsService {
    @Autowired
    private UserDao userDao;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Fuser byUsername = userDao.findByUsername(username);
        if(byUsername==null){
            throw  new UsernameNotFoundException("用户名没有找到");
        }
        byUsername.setDisplayName(byUsername.getFuid().toString());
       // byUsername.setFuid(byUsername.getFuid());
        System.out.println("我存入的user"+byUsername);
        return  byUsername;
    }

    public static void main(String[] args) {
        BCryptPasswordEncoder passwordEncoder = new    BCryptPasswordEncoder();
        String password = passwordEncoder.encode("123456");
        System.out.println(password);
    }

}
