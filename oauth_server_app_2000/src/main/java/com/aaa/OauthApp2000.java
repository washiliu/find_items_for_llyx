package com.aaa;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
/*
* 用户认证微服务启动类
* */
@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("com.aaa.dao")
public class OauthApp2000 {
    public static void main(String[] args) {
        SpringApplication.run(OauthApp2000.class,args);
    }
}
