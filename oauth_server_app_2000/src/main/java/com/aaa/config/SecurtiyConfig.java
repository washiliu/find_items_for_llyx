package com.aaa.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;


/**
 * security配置
 */
@Configuration
@EnableWebSecurity

public class SecurtiyConfig extends WebSecurityConfigurerAdapter {
    /**
     * 在容器中放AuthenticationManager对象 等一下oauth2配置文件中要使用这个对象
     * @return
     * @throws Exception
     */
    @Bean
    protected AuthenticationManager getManager() throws Exception {
        return super.authenticationManager();
    }
}
