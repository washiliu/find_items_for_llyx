package com.aaa.dao;

import com.aaa.entity.Fuser;

/**
 * 用户管理到接口
 */
public interface UserDao {
    /**
     * 根据用户名查询用户
     * @param username
     * @return
     */
    Fuser findByUsername(String username);

}
