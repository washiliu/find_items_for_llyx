package com.aaa.controller;

import com.aaa.entity.Fuser;
import com.aaa.entity.Kill;
import com.aaa.service.CecilService;
import com.aaa.util.DefaultMsg;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.xml.transform.Result;
import java.util.HashMap;
import java.util.concurrent.Future;

@RestController
@RequestMapping("/ms")
public class CeliController {
    @Autowired
    private CecilService cecilService;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    private Object getUser(Authentication authentication) {
        Object principal = authentication.getPrincipal();
        Fuser byFuname = cecilService.findByFuname(principal.toString());
        return byFuname;
    }

    /**
     * @author Bc
     * 是否存在秒杀商品
     * 加入到Redis 缓存中
     * @param: []
     */
    @RequestMapping("/selectByPids")
    public DefaultMsg selectByPids() {
        return cecilService.listAll();
    }

    /**
     * @author Bc
     *  秒杀商品
     * @return
     */
    @RequestMapping("/listAll")
    public DefaultMsg listAll() {
        DefaultMsg defaultMsg = cecilService.listAll();
        return defaultMsg;
    }

    //后端接收秒杀请求的接口
    @RequestMapping(value = "doKill", method = RequestMethod.POST)
    public DefaultMsg doMiaosha(Integer pid,Authentication authentication) {
        Fuser fuser =(Fuser)this.getUser(authentication);
        DefaultMsg defaultMsg=new DefaultMsg();
        //1.如果用户为空，则返回至登录页面
        if (fuser == null) {
            return new DefaultMsg("请先登录");
        }
        //2.预减少库存，减少redis里面的库存
        Boolean reduceStock = cecilService.reduceStock(pid, 1);
        //3.判断减少数量1之后的stock，区别于查数据库时候的stock<=0
        if (reduceStock ==false) {
            return new DefaultMsg("秒杀结束");
        }
        //4.判断这个秒杀订单形成没有，判断是否已经秒杀到了，避免一个账户秒杀多个商品
        Boolean success = cecilService.isSuccess(Math.toIntExact(fuser.getFuid()), pid);
        if (success != false) {// 查询到了已经有秒杀订单，代表重复下单
            return new DefaultMsg("重复下单");
        }else {
            //5.正常请求，入队，发送一个秒杀 normalQueue1 到队列里面去，入队之后客户端应该进行轮询。
            //5.1 入队
            rabbitTemplate.convertAndSend("normalQueue1", fuser.getFuid()  + pid);
            //返回0代表排队中
            defaultMsg.setCode(0);
        }

        return defaultMsg;
    }


}
