package com.aaa.listener;

import com.aaa.entity.Kill;
import com.aaa.service.CecilService;
import com.aaa.service.impl.CecilServiceImpl;
import com.alibaba.fastjson.JSON;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class DirectReceiver {
    @Autowired
    CecilService cecilService;


    @RabbitListener(queues = "normalQueue1")
    public void readFromBootMaliQueue3(Kill kill, Message message, Channel channel) throws Exception {
        System.out.println("接受到信息：" + kill);
        //手动确认信息
        channel.basicAck(message.getMessageProperties().getDeliveryTag(), true);
    }

}
