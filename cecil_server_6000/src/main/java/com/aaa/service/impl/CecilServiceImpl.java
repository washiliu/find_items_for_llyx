package com.aaa.service.impl;

import com.aaa.dao.CecilDao;
import com.aaa.entity.Fuser;
import com.aaa.entity.ProDisCount;
import com.aaa.service.CecilService;
import com.aaa.util.DefaultMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CecilServiceImpl implements CecilService {
    @Autowired
    private CecilDao cecilDao;
    @Autowired
    private StringRedisTemplate redisTemplate;


    /**
     * 查询所有秒杀的信息
     * @return
     */
    @Override
    public DefaultMsg listAll() {
        List<ProDisCount> proDisCounts = cecilDao.listAll();
        DefaultMsg defaultMsg = new DefaultMsg();
        if (proDisCounts.size()==0){
            defaultMsg.setSuccess(0);
            defaultMsg.setMsg("没有优惠商品");
        }else {
            //存在秒杀商品储存 redis 中
            for (ProDisCount proDisCount : proDisCounts) {
                redisTemplate.opsForValue().set("item::stock"+proDisCount.getPid(),proDisCount.getNums());
            }
            defaultMsg.setSuccess(1);
            defaultMsg.setMsg("有优惠商品");
            defaultMsg.setTarget(proDisCounts);

        }
        return defaultMsg;
    }

    /**
     * 扣减缓存商品数量
     * @param pid
     * @param nums
     * @return
     */
    @Override
    public Boolean reduceStock(Integer pid, int nums) {
        //判断参数是否合法
        if (pid<=0||nums<=0){
            return false;
        }
        String key="item::stock"+pid;
        long stock = redisTemplate.opsForValue().decrement(key, nums);
        if (stock < 0) {
            //回补库存
            redisTemplate.opsForValue().increment(key, nums);
        }else if(stock==0) {
            //售罄
            return false;
        }
        return stock>=0;
    }

    @Override
    public Fuser findByFuname(String funame) {
        Fuser byFuname = cecilDao.findByFuname(funame);
        return byFuname;
    }
    @Override
    public Boolean isSuccess(Integer fid, Integer pid) {
        Boolean success = cecilDao.isSuccess(fid, pid);
        return success;
    }

    @Override
    public Integer saveCecil(Integer fid,Integer pid) {
        Integer integer = cecilDao.saveCecil(fid, pid);
        return integer;
    }
}
