package com.aaa.service;

import com.aaa.entity.Fuser;
import com.aaa.util.DefaultMsg;

public interface CecilService {

    //查询所有秒杀商品信息
    DefaultMsg listAll();
    //扣减商品库存
    Boolean reduceStock(Integer pid, int nums);
    //查询信息
    Fuser findByFuname(String funame);
    //是否秒杀成功
    Boolean isSuccess(Integer fid,Integer pid);
    //新增秒杀订单
    Integer saveCecil(Integer fid,Integer pid);


}
