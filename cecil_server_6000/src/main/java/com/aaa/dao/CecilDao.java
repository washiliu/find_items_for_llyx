package com.aaa.dao;

import com.aaa.entity.Fuser;
import com.aaa.entity.Orders;
import com.aaa.entity.ProDisCount;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @author Bc
 * 秒杀商品接口
 */
public interface CecilDao {
    //查询是否存在秒杀的商品
    Integer selectByPid();

    //查询所有秒杀商品信息
    List<ProDisCount> listAll();
    //查询用户
    Fuser findByFuname(String funame);
    //是否秒杀过
    Boolean isSuccess(@Param("fid") Integer fid, @Param("pid") Integer pid);
    //查询订单信息
    Orders findOreder(Integer fid);
    //新增秒杀订单
    Integer saveCecil(@Param("fid") Integer fid, @Param("pid") Integer pid);
}
