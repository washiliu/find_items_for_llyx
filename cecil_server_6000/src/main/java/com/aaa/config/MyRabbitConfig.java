package com.aaa.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * RabbitMq配置类
 */
@Configuration
public class MyRabbitConfig {
    //死信交换机名称
    public  static  final  String Y_DEAD_LETTER_EXCHANGE="Y";
    /**
     * 创建死信队列
     * @return
     */
    @Bean
    public Queue  dlQueue1(){
        return  new Queue("dlQueue1");
    }

    /**
     * 创建Fanout类型交换机
     * 死信交换机
     * @return
     */
    @Bean
    public FanoutExchange dllExcjange1() {

        return new FanoutExchange(Y_DEAD_LETTER_EXCHANGE);
    }

    /**
     * 绑定dlQueue1(死信队列)队列到bootdllExcjange1(死信交换机)交换机上
     *
     * @param dlQueue1
     * @return
     */
    @Bean
    public Binding bindingMailFanoutExchange(Queue dlQueue1, FanoutExchange dllExcjange1) {
        return BindingBuilder.bind(dlQueue1).to(dllExcjange1);
    }

    /**
     * 创建正常队列
     * @return
     */
    @Bean
    public Queue  normalQueue1(){
        Map<String,Object> map=new HashMap<>();
        //设置队列中的信息未被消费则60秒后过期
        map.put("x-message-ttl",60000);
        map.put("x-dead-letter-exchange",Y_DEAD_LETTER_EXCHANGE);
        return  new Queue("normalQueue1",true,false,false,map);
    }

    /**
     * 创建Fanout类型交换机
     * 正常交换机
     * @return
     */
    @Bean
    public FanoutExchange normalExchange1() {
        Map<String, Object> map = new HashMap();
        return new FanoutExchange("/normalExchange1", true, true, map);
    }

    /**
     * 绑定normalQueue1(正常队列)队列到normalExchange1(正常交换机)交换机上
     *
     * @param normalQueue1
     * @param normalExchange1
     * @return
     */
    @Bean
    public Binding bindingMailFanoutExchange1(Queue normalQueue1, FanoutExchange normalExchange1) {
        return BindingBuilder.bind(normalQueue1).to(normalExchange1);
    }
}
